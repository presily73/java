public class Geometry3D {

    public static double calculateCuboidVolume(double length, double width, double height) {
        return length * width * height;
    }
    public static double calculateSurfaceArea(double length, double width, double height) {
        double surfaceArea = 2 * ((length * width) + (length * height) + (width * height));
        return surfaceArea;
    }
    // method to calculate the volume of a square based pyramid
    public static double calculateSquarePyramidVolume(double baseSide, double height) {
        return (1.0/3.0) * (baseSide * baseSide) * height;
    }
    // Calculate the surface area of a square based pyramid
    public static double surfaceAreaOfSquarePyramid(double a, double h) {
        double sa = Math.pow(a, 2) + a * Math.sqrt(Math.pow(a, 2) + 4 * Math.pow(h, 2));
        return sa;
    }
    public static double volumeOfTetrahedron(double sideLength) {
        double numerator = Math.pow(sideLength, 3);
        double denominator = 6.0 * Math.sqrt(2.0);
        return numerator / denominator;
    }
    public static double surfaceAreaOfTetrahedron(double side) {
        return Math.sqrt(3) * (side * side);}

}

