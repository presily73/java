import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class Geometry3DTest {

    @Test
    public void testCalculateCuboidVolume() {
        double length = 2.5;
        double width = 3.0;
        double height = 4.0;

        double expectedVolume = 30.0;
        double actualVolume = Geometry3D.calculateCuboidVolume(length, width, height);

        assertEquals(expectedVolume, actualVolume, 0.0001);
    }
    @Test
    public void testCalculateSurfaceArea() {
        double length = 5.0;
        double width = 3.0;
        double height = 2.0;
        double expectedSurfaceArea = 62.0;
        double delta = 0.0001;  // tolerance for floating-point equality
        double actualSurfaceArea = Geometry3D.calculateSurfaceArea(length, width, height);
        assertEquals(expectedSurfaceArea, actualSurfaceArea, delta);
    }

    @Test
    public void testCalculateSquarePyramidVolume() {
        double baseSide = 4.0;
        double height = 5.0;
        double expectedVolume = 26.666666666666668;
        double volume = Geometry3D.calculateSquarePyramidVolume(baseSide, height);
        assertEquals(expectedVolume, volume, 0.0001);
    }
    @Test
    public void testSurfaceAreaOfSquarePyramid() {
        double expected = 34.63201123595259;
        double actual = Geometry3D.surfaceAreaOfSquarePyramid(3, 4);
        assertEquals(expected, actual, 0.01);
    }

    @Test
    public void testVolumeOfTetrahedron() {
        double sideLength = 5.0;
        double expectedVolume = (sideLength * sideLength * sideLength) / (6.0 * Math.sqrt(2.0));
        double actualVolume = Geometry3D.volumeOfTetrahedron(sideLength);
        assertEquals(expectedVolume, actualVolume, 0.01);
    }
    @Test
    public void testSurfaceAreaOfTetrahedron() {
        double side = 5;
        double expected = Math.sqrt(3) * (side * side);
        double actual = Geometry3D.surfaceAreaOfTetrahedron(side);
        assertEquals(expected, actual, 0.01);
    }

}
